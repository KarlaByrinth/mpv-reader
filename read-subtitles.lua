local active = false

function handle_sub_text_change(_, sub_text)
	if sub_text ~= nil and sub_text ~= "" then
		mp.osd_message("read subtitle")
		os.execute("espeak -p 30 -ven-german-5 -s 255 -g 1  " .. '"' .. sub_text .. '"')
	end
end

function display_state()
	local msg
	if active then
		msg = "Read subtitles enabled"
	else msg = "Read subtitles disabled" end
	mp.osd_message(msg)
end

function toggle()
	if active then
		mp.unobserve_property(handle_sub_text_change)
		active = false
	else
		mp.observe_property("sub-text", "string", handle_sub_text_change)
		active = true
	end
	display_state()
end

mp.add_key_binding(nil, "sub-pause-toggle-start", function()
	toggle()
end)

mp.add_key_binding("n", "sub-pause-toggle-end", function()
	toggle()
end)
