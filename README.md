# Read Text from clipboard with screenreader with mpv

## command:

xterm -e python mpv-reader.py

## example for xbindkeys, xbindkeyrc:

"xterm -e python mpv-reader.py"
    m:0x8 + c:27
        Alt + r

## ideas

VTE is a component of GTK and probably could be used for popup windows as a terminal

## external links:

[festival documentation](http://www.festvox.org/docs/manual-2.4.0/festival_toc.html)

## credits

blinry helped me a lot during a pair programming session.

# Read subtitles in mpv aloud

## command:

mpv --script=/$path/$to/mpv-reader/read-subtitles.lua $movie.webm

activate with keybinding 'n'

## credits

read-subtitles.lua is inspired by and partially based on mpv-sub-scripts from [a github-project by Ben-Kerman](https://github.com/Ben-Kerman/mpv-sub-scripts). Also, Leonard helped me a lot.
